import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		redirect: '/characters',
	},
	{
		path: '/characters',
		name: 'Home',
		component: Home,
	},
    {
		path: '/characters/:id',
		name: 'CharacterPage',
        component: () => import('@/views/CharacterPage.vue'),
	},
	{
		path: '*',
		name: '404',
		component: () => import('@/views/404.vue'),
	},
];

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
});

export default router;
