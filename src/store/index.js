import Vue from 'vue';
import Vuex from 'vuex';
import { sortedUniqBy, sortBy } from 'lodash';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		characters: [],
		paginateCharacters: [],
		paginationSize: 20,
		currentPage: 1,
		totalPages: 1,
		filters: {
			name: '',
			status: null,
		},
		loading: false,
	},
	getters: {
		getFilters: (state) => {
			return state.filters;
		},
		getPaginateCharacters: (state) => {
			return state.paginateCharacters;
		},
		getTotalPages: (state) => {
			return state.totalPages;
		},
		getCurrentPage: (state) => {
			return state.currentPage;
		},
		getLoading: (state) => {
			return state.loading;
		},
	},
	mutations: {
		setFilterStatus(state, status) {
			state.filters.status = status;
		},
		setCharacters(state, characters) {
			state.characters = characters;
		},
		setPaginateCharacter(state, paginateCharacters) {
			state.paginateCharacters = paginateCharacters;
		},
		setTotalPages(state, total) {
			state.totalPages = total;
		},
		setCurrentPage(state, pageNumber) {
			state.currentPage = pageNumber;
		},
		setLoading(state, boolean) {
			state.loading = boolean;
		},
	},
	actions: {
		//First load of all characters
		async loadCharacters({ commit, state }) {
			if (state.characters.length > 0) {
				return;
			}

			commit('setLoading', true);

			const totalPages = await Vue.prototype.$rickAndMortyAPI
				.get('/character')
				.then((res) => res.data.info.pages);

			//Generate urls for fetching datas
			let urls = [];
			for (let index = 1; index <= totalPages; index++) {
				urls.push('/character?page=' + index);
			}

			//Fetching datas
			let results = [];
			await Promise.all(
				urls.map(async (url) => {
					return await Vue.prototype.$rickAndMortyAPI
						.get(url)
						.then((res) => {
							results = [...results, ...res.data.results];
						});
				})
			);

			results = sortBy(results, ['id']);

			commit('setCharacters', results);
			commit('setLoading', false);
		},
		paginateCharacters({ commit, state }) {
			let characters = state.characters;

			//Status filter
			if (state.filters.status) {
				characters = state.characters.filter(
					(char) => char.status === state.filters.status
				);
			}

			//Filter by name(s)
			if (state.filters.name) {
				const searchWords = state.filters.name.split(',');

				let filteredCharacters = [];
				searchWords.map((word) => {
					let results = word
						? characters.filter((char) =>
								char.name
									.toLowerCase()
									.includes(word.trim().toLowerCase())
						  )
						: [];
					filteredCharacters = [...filteredCharacters, ...results];
				});

				characters = sortedUniqBy(filteredCharacters, 'id');
			}

			//Paginate results
			const paginateCharacters = characters.slice(
				(state.currentPage - 1) * state.paginationSize,
				state.currentPage * state.paginationSize
			);

			let totalPages = Math.trunc(
				characters.length / state.paginationSize
			);

			const modulo = characters.length % state.paginationSize;

			if (modulo) {
				totalPages += 1;
			}

			commit('setPaginateCharacter', paginateCharacters);
			commit('setTotalPages', totalPages);
		},
		changeCurrentPage({ dispatch, commit }, pageNumber) {
			commit('setCurrentPage', pageNumber);
			dispatch('paginateCharacters');
		},
		changeFilterStatus({ dispatch, commit }, status) {
			commit('setFilterStatus', status);
			dispatch('changeCurrentPage', 1);
		},
	},
	modules: {},
});
